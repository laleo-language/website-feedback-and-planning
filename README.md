# Website feedback and planning

Welcome to Laleo Languages feedback and planning!

Please use this area to report issues with the website or suggest improvements.

To open a new issue, you first need to sign in; you can sign in with Google, or create an account yourself.

Once you're logged in, click the `+` at the top left corner this page, and under "In this project" select "New issue".

Thanks for your support and feedback!

